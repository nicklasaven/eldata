
/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
CREATE ROLE eldata_read;
CREATE ROLE eldata_writer;

CREATE ROLE eldata WITH LOGIN;
GRANT eldata_reader, eldata_writer TO eldata;  --För att i go-servern kunna byta till rätt roll



CREATE SCHEMA IF NOT EXISTS elmatare;

GRANT USAGE ON SCHEMA elmatare TO eldata_reader;
GRANT USAGE ON SCHEMA elmatare TO eldata_writer;




CREATE TABLE IF NOT EXISTS elmatare.matdata
(
    "0-0:1.0.0" timestamp without time zone,
    "1-0:1.8.0" double precision,
    "1-0:2.8.0" double precision,
    "1-0:3.8.0" double precision,
    "1-0:4.8.0" double precision,
    "1-0:1.7.0" double precision,
    "1-0:2.7.0" double precision,
    "1-0:3.7.0" double precision,
    "1-0:4.7.0" double precision,
    "1-0:21.7.0" double precision,
    "1-0:41.7.0" double precision,
    "1-0:61.7.0" double precision,
    "1-0:22.7.0" double precision,
    "1-0:42.7.0" double precision,
    "1-0:62.7.0" double precision,
    "1-0:23.7.0" double precision,
    "1-0:43.7.0" double precision,
    "1-0:63.7.0" double precision,
    "1-0:24.7.0" double precision,
    "1-0:44.7.0" double precision,
    "1-0:64.7.0" double precision,
    "1-0:32.7.0" double precision,
    "1-0:52.7.0" double precision,
    "1-0:72.7.0" double precision,
    "1-0:31.7.0" double precision,
    "1-0:51.7.0" double precision,
    "1-0:71.7.0" double precision,
    created timestamp with time zone DEFAULT now(),
    created_epoch bigint DEFAULT EXTRACT(epoch FROM now()),
    gid serial primary key,
    CONSTRAINT "matdata_0-0:1.0.0_key" UNIQUE ("0-0:1.0.0")
);


GRANT SELECT ON TABLE elmatare.matdata TO eldata_reader;
GRANT INSERT, SELECT ON TABLE elmatare.matdata TO eldata_writer;
GRANT USAGE ON SEQUENCE elmatare.matdata_gid_seq TO eldata_writer;


CREATE INDEX IF NOT EXISTS idx_matdata_created
    ON elmatare.matdata USING btree (created);

CREATE INDEX IF NOT EXISTS idx_matdata_created_epoch
    ON elmatare.matdata USING btree (created);

CREATE INDEX IF NOT EXISTS idx_matdata_measured_bought
    ON elmatare.matdata USING btree
    (round("1-0:1.8.0"::numeric, 3));

CREATE INDEX IF NOT EXISTS idx_matdata_measured_sold
    ON elmatare.matdata USING btree
    (round("1-0:2.8.0"::numeric, 3) );




CREATE OR REPLACE VIEW elmatare.selected
 AS
 SELECT matdata.gid,
    matdata."0-0:1.0.0" AS "timestamp",
    matdata.created,
    round(matdata."1-0:1.8.0"::numeric, 3) AS measured_bought,
    round(matdata."1-0:2.8.0"::numeric, 3) AS measured_sold,
    round(matdata."1-0:1.7.0"::numeric, 3) AS power_buying,
    round(matdata."1-0:2.7.0"::numeric, 3) AS power_selling,
    matdata.created_epoch
   FROM elmatare.matdata;

GRANT SELECT ON TABLE elmatare.selected TO eldata_reader;





-- FUNCTION: elmatare.calc_range(bigint, bigint, integer)

-- DROP FUNCTION IF EXISTS elmatare.calc_range(bigint, bigint, integer);

CREATE OR REPLACE FUNCTION elmatare.calc_range(
	start_epoch bigint,
	stop_epoch bigint,
	n_vals integer)
    RETURNS SETOF elmatare.selected
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
declare
    q elmatare.selected;
    _diff int;
begin
    _diff = ((stop_epoch - start_epoch)/(n_vals))::numeric;
    if _diff <= 10 then
        FOR q in
        SELECT *
        FROM elmatare.selected where created_epoch > start_epoch and created_epoch <= stop_epoch order by gid
            LOOP
                RETURN NEXT q;
        END LOOP;
        return;
    end if;

    FOR q in
        with temp_vals as
    (
        select gid,min(gid) OVER (ROWS 1 PRECEDING EXCLUDE CURRENT ROW) pre_gid, created_epoch, measured_bought, measured_sold from  elmatare.selected e1 where created_epoch > start_epoch and created_epoch <= stop_epoch and created_epoch % _diff < 10 order by gid
    )
    SELECT distinct on (t1.gid) t1.gid, null as timestamp,
        to_timestamp(t1.created_epoch + (t2.created_epoch - t1.created_epoch)/2) created,
        round((t1.measured_bought + (t2.measured_bought - t1.measured_bought)/2)::numeric, 3) measured_bought,
        round((t1.measured_sold + (t2.measured_sold - t1.measured_sold)/2)::numeric, 3) measured_sold,
        round(((t2.measured_bought - t1.measured_bought) * 3600 / (T2.created_epoch-T1.created_epoch))::numeric, 3) power_buying,
        round(((t2.measured_sold - t1.measured_sold) * 3600 / (T2.created_epoch-T1.created_epoch))::numeric, 3) power_selling,
        t1.created_epoch + (t2.created_epoch - t1.created_epoch)/2 created_epoch
    FROM temp_vals t1 inner join temp_vals t2 on t1.pre_gid = t2.gid order by t1.gid
        LOOP
        RETURN NEXT q;
    END LOOP;
end;
$BODY$;
