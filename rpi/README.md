**Raspberry Pi**

Jag har använt en rpi3 med wifi. När den har läst in data från elmätaren så skickar den data via ett 4G-modem jag har i närheten upp till databas-servern. Har man möjlighet för nätverkssladd eller wifi till hemnätverket så är det ju lite enklare. 

Tänk bara på att om du exponerar ditt hemnätverk ute vid elskåpet så är det en attackpunkt för någon som vill komma in på ditt nätverk. Om du använder wifi så kommer passordet till ditt nätverk att finnas i raspberryn. Även om du har inloggningen passordsskyddad så är det bara att stoppa kortet i en vanlig dator och läsa det om du inte har krypterat kortet. 

I mitt fall så är 4G-nätverket separat från mitt hemnätverk och mina datorer inomhus.

**Uppsätt av rpi**

Jag la in ett standard Debian OS i min rpi. Det räcker med terminal, grafik behövs inte. Så får man sätta upp så rpin har tillgång till nätverk vilken väg man ny väljer.

**Program som behövs i rpin**

För att bygga C-program i rpin så är det lättaste att installera paketet 
``build-essential``

Om man inte vill bygga allt själv så är det lättaste att installera dessa paket från apt:

```
libcurl4-openssl-dev
libserialport-dev
```

Så är det 2 saker som måste editeras i filen send.c:
på rad 51 är det ett påhittat token. Det är inget äkta token utan bara en sträng som go-servern sedan kan känna igen för att veta att data kommer från dig. Risken annars är att någon annan fyller din databs med spam. Samma sträng som du lägger in där skall också in i go-servern i filen utils.go på rad 39. Detta är bara ett fult hack för att öka säkerheten litegrann. 

Så behöver du lägga in url till din egen db-server på rad 57 i send.c


Sedan är det bara att kopiera över filerna i den här foldern till rpin och bygga med:

```
gcc send.c read.c main.c -lcurl -lserialport -o matarcollector -g
```

Så har jag gjort så att jag har kopierat filen som skapas "matarcollector" till /usr/local/bin
Så har jag satt upp en service som kör programmet. 

Sedan är det bara att plugga in rj12-kabeln i mätaren och via usb-adaptern i rpin så skall data skickas till din server.