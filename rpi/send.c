/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "serial.h"
#include <curl/curl.h>

int add_header(struct curl_slist **header_list, char *val)
{    
    struct curl_slist *tmp_list = NULL;
    tmp_list = curl_slist_append(*header_list, val);
    if (!tmp_list)
    {        
        curl_slist_free_all(*header_list);
        return -1;
    }
    *header_list = tmp_list;
    return 0;
}

int send_data(char *data, int strlength)
{
    CURL *curl = curl_easy_init();
    if (!curl)
    {
        printf("No curl");
     return 1;
    }
    
    CURLcode res;
  
    struct curl_slist *header_list = NULL;
    
    //Here we misuse the token mechanisms. Since we have full control over both sender and receiver and no one else will be onvolved
    // we just add something as token and check that the same text is comming on each request to the server. There is no reason to 
    //put effort in signing the token since the content will not change so we can just verify the full token as is.
    if (add_header(&header_list, "Authorization: Bearer ThisIsTheSameTextAsFoundInThOtherEndOfTheCommunication"))
        return -1;
    if (add_header(&header_list,"Content-Type: application/json" ))  
        return -1;
    
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header_list);
   curl_easy_setopt(curl, CURLOPT_URL, "https://url.to.server");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, (long) strlength);

    res = curl_easy_perform(curl);
    curl_slist_free_all(header_list);
    /* Check for errors */
    if(res != CURLE_OK)
    {
        printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));    
        return 1;
    }
    
    /* always cleanup */
    curl_easy_cleanup(curl);
    
    return 0;
}
