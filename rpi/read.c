/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "serial.h"

/*
int main()
{
char ch;
char buf[4096];
int bufsize = sizeof(buf);
memset(buf, 0, bufsize);

   FILE *fp;
   fp = fopen("res.txt", "r"); // read mode

   if (fp == NULL)
   {
      perror("Error while opening the file.\n");
      exit(EXIT_FAILURE);
   }
int c = 0;
   int read_cursor = 0;
   int first_of_line = 1;
   int in_msg  = 0;   
   
   while((ch = fgetc(fp)) != EOF)
   {
	   
	if (first_of_line)
	{
		if (ch == '1' || ch == '0')
		{
			
			in_msg = 1;
		}
		   if (ch == '!')
		   {
               
				buf[read_cursor++] = '\0';
                read_data(buf, read_cursor);
			   read_cursor = 0;
			   in_msg = 0;
		   }
		   first_of_line = 0;
	}
	
	if(in_msg)
	{		
		if (read_cursor<bufsize -2 && ch != '\0')
		{
			buf[read_cursor++] = ch;
		}
	}
	   c++;
		
	   if (ch == '\n')
	   { 
		   first_of_line = 1;
	   }
   }
   buf[c] = '\0';

   fclose(fp);

   return 0;
}
*/

int read_data(char *str, int strlength)
{

	int i;
	int first_of_line = 1;
	int in_msg  = 0;

	char resbuf[4096];
	char *res_cursor = resbuf;
    
    resbuf[0] = '{';
    res_cursor++;
	for (i=0; i<strlength;i++) 
   {
	   
	if (first_of_line)
	{
		first_of_line = 0;
		if (str[i] == '0')
		{
			i += read_date(str + i, &res_cursor);
		} 
		else if (str[i] == '1')
		{
			i+= read_val(str + i, &res_cursor);
		}
	}
		
		
	   if (str[i] == '\n')
	   { 
		first_of_line = 1;
	   }
   }
   
    *res_cursor = '}';
    res_cursor++;
    *res_cursor = '\0';
    res_cursor++;
    send_data(resbuf, resbuf - res_cursor);
   	return 0;
}



int read_date(char *str, char **resbuf)
{
    int i = 0;    
    char *resbuf_cursor = *resbuf;
    
    char *txt = "\"";
    int txtlen = strlen(txt);
    strncpy(resbuf_cursor, txt,txtlen);
    resbuf_cursor += txtlen;
    while (str[i] != '\n')
    {
        
        if (str[i] == '(')
        {
            
            char *txt = "\":\"";
            int txtlen = strlen(txt);
            strncpy(resbuf_cursor, txt,txtlen);
            resbuf_cursor += txtlen;
                i++;
        }
        if (str[i] == 'X' ||str[i] == 'W')
        {
            
            char *txt = "\"";
            int txtlen = strlen(txt);
            strncpy(resbuf_cursor, txt,txtlen);
            resbuf_cursor += txtlen;
            *resbuf = resbuf_cursor;
            return i;
        }

        *resbuf_cursor = str[i];
        resbuf_cursor++;
        
        i++;
        
    }
    
    
    
	return 0;
}

int read_val(char *str, char **resbuf)
{
	
    int i = 0;   
    int delzeros = 0;
    char *resbuf_cursor = *resbuf;
    
    char *txt = ",\"";
    int txtlen = strlen(txt);
    strncpy(resbuf_cursor, txt,txtlen);
    resbuf_cursor += txtlen;
    while (str[i] != '\n')
    {
        
        if (str[i] == '(')
        {
            delzeros = 1;
            char *txt = "\":{\"val\":";
            int txtlen = strlen(txt);
            strncpy(resbuf_cursor, txt,txtlen);
            resbuf_cursor += txtlen;
                i++;
        }
        
        if (str[i] == '*')
        {            
            delzeros = 0;
            char *txt = ",\"unit\":\"";
            int txtlen = strlen(txt);
            strncpy(resbuf_cursor, txt,txtlen);
            resbuf_cursor += txtlen;
                i++;
        }
        if (str[i] == ')')
        {
            delzeros = 0;
            char *txt = "\"}";
            int txtlen = strlen(txt);
            strncpy(resbuf_cursor, txt,txtlen);
            resbuf_cursor += txtlen;
            *resbuf = resbuf_cursor;
            return i;
        }
        if (delzeros)
        {
            while (str[i] == '0' && str[i+1] != '.' )
                i++;
            
            delzeros = 0;
        }
        *resbuf_cursor = str[i];
        resbuf_cursor++;
        
        i++;
        
    }    
	return 0;
}
