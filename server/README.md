**Go Server för att samla och serva elmätardata**

En liten server skriven i GO för att ta emot elmätardata samt att serva ut data till websida.

Inspiration till websocketlösningen är hämtad från 
https://dev.to/danielkun/go-asynchronous-and-safe-real-time-broadcasting-using-channels-and-websockets-4g5d

Websidans html-kod ligger inbakad i sidan dataout.go, men kan naturligtvis lika gärna servas separat. 

OBS! I html-sidan som ligger i dataout.go så måste man ändra till sin egen web-address på rad 153
``const baseUrl = "Set.Your.HostName.Here"``

Om du använder en http-address istället för https så måste du också ändra protokoll från wss till ws på rad 471
Den sätter automatiskt ws-protokoll om det är localhost, men annars måste man ändra.

Så behöver du sätta en tokensträng som är likadan som i filen send.c in i filen utils.go. Detta är bara ett hack för att go-servern skall kunna verifiera att data kommer från dig.


websidan är inte fin och alla bidrag till att förbättra den mottages tacksamt. 

