/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func (p *GoServer) router() http.Handler {
	r := mux.NewRouter()

	// Log when an appengine warmup request is used to create the new instance.
	// Warmup steps are taken in setup for consistency with "cold start" instances.
	http.HandleFunc("/_ah/warmup", func(w http.ResponseWriter, r *http.Request) {
		log.Println("warmup done")
	})

	r.HandleFunc("/postElMatarData/", p.elMatarData)
	r.HandleFunc("/eldata", p.elData)
	r.HandleFunc("/getData", p.getData)
	r.HandleFunc("/pushData", p.pushData)
	r.HandleFunc("/getDataTimeRange", p.getDataTimeRange)
	notFound := NotFoundHandler{}
	r.NotFoundHandler = &notFound
	return r
}

type NotFoundHandler struct{}

func (n *NotFoundHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	http.Error(w, "404: unknown url "+r.URL.String(), http.StatusNotFound)
}
