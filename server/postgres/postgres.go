/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// Package postgres contains helper functions for reading and posting to PostgreSQL databases
package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"log"
	"strings"
	"time"
)

// Db is a struct holding information about the database.
// It also contains the login configuration for reference
type Db struct {
	pool   *pgxpool.Pool
	DbConf *DBConf
}

type DBConf struct {
	GoogleProject string `json:"google_project"`
	DBHost        string `json:"DBHost"`
	DBPort        int    `json:"DBPort"`
	DBLoginUser   string `json:"DBLoginUser"`
	DBReadUser    string `json:"DBReadUser"`
	DBWriteUser   string `json:"DBWriteUser"`
	DBName        string `json:"DBName"`
	DBpw          []byte
}

// RenameForPostgres makes column names lower case and replaces special characters
func RenameForPostgres(str string) string {

	str = strings.Replace(str, " ", "_", -1)
	str = strings.Replace(str, ".", "_", -1)
	str = strings.Replace(str, ",", "_", -1)
	str = strings.Replace(str, "(", "", -1)
	str = strings.Replace(str, ")", "", -1)
	str = strings.Replace(str, "ø", "o", -1)
	str = strings.Replace(str, "Ø", "O", -1)
	str = strings.Replace(str, "æ", "a", -1)
	str = strings.Replace(str, "Æ", "A", -1)
	str = strings.Replace(str, "å", "a", -1)
	str = strings.Replace(str, "Å", "A", -1)
	str = strings.Replace(str, " - ", "_", -1)
	str = strings.Replace(str, "-", "_", -1)
	str = strings.Replace(str, "/", "_", -1)

	str = strings.ToLower(str)
	return str
}

func (db *Db) SetDbConf() (err error) {

	log.Printf("Entering SetDbConf\n")
	conf := db.DbConf

	db.DbConf.DBHost = conf.DBHost
	db.DbConf.DBPort = conf.DBPort
	db.DbConf.DBLoginUser = conf.DBLoginUser
	db.DbConf.DBReadUser = conf.DBReadUser
	db.DbConf.DBWriteUser = conf.DBWriteUser
	db.DbConf.DBpw = conf.DBpw
	db.DbConf.DBName = conf.DBName

	return nil
}

func PostgresQuoteStrList(s string) (n int, res string) {
	sArray := strings.Split(s, ",")
	n = len(sArray)
	for i, str := range sArray {
		if i > 0 {
			res = res + ","
		}
		res = res + "'" + str + "'"
	}
	return n, res
}

func (db *Db) OpenDb() error {

	//Already connected
	if db.pool != nil {
		return nil
	}

	if db.DbConf == nil {
		log.Fatalf("Config not set\n")
	}

	connInfo := db.DbConf

	var psqlconn string

	if len(connInfo.DBName) > 0 {
		psqlconn = fmt.Sprintf("%s database=%s", psqlconn, connInfo.DBName)
	} else {
		return fmt.Errorf("Missing Database Name")
	}
	if len(connInfo.DBHost) > 0 {
		psqlconn = fmt.Sprintf("%s host=%s", psqlconn, connInfo.DBHost)
	}
	if connInfo.DBPort > 0 {
		psqlconn = fmt.Sprintf("%s port=%d", psqlconn, connInfo.DBPort)
	}
	if len(connInfo.DBLoginUser) > 0 {
		psqlconn = fmt.Sprintf("%s user=%s", psqlconn, connInfo.DBLoginUser)
	}
	if len(connInfo.DBpw) > 0 {
		psqlconn = fmt.Sprintf("%s password=%s", psqlconn, connInfo.DBpw)
	}
	log.Printf("psqlconn: %s\n", psqlconn)
	config, err := pgxpool.ParseConfig(psqlconn)
	config.MaxConns = 200
	pool, err := pgxpool.ConnectConfig(context.TODO(), config)
	//	pool, err := pgxpool.Connect(context.Background(), psqlconn)
	db.pool = pool
	if err != nil {

		log.Printf("Error: %s\n", err)
		log.Fatalf("Unable to connect to database: %v\n", err)
		//		os.Exit(1)
	}

	log.Printf("return from openDb")
	return nil
}

func setRole(tx pgx.Tx, role string) (err error) {
	role = strings.Replace(role, ";", "", -1)
	_, err = tx.Exec(context.TODO(), fmt.Sprintf("SET ROLE %s;", role))
	return nil
}

func resetRole(tx pgx.Tx) (err error) {
	_, err = tx.Exec(context.TODO(), "RESET ROLE;")
	return nil
}

func commitResetRole(tx pgx.Tx) (err error) {
	err = resetRole(tx)
	if err != nil {
		rollbackResetRole(tx)
		return err
	}
	err = tx.Commit(context.TODO())
	return
}

func (db *Db) CloseConn() {
	if db.pool != nil {
		db.pool.Close()
		db.pool = nil
	}
	return
}

func (db *Db) getTxSetRole(role string) (tx pgx.Tx, err error) {
	err = db.OpenDb()
	if err != nil {
		return tx, err
	}

	//If all connections in connection pool are used we get an error.
	// Then we retry 30 times with 0.1 second in between
	for i := 0; i < 30; i++ {
		tx, err = db.pool.Begin(context.TODO())
		if err == nil {
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
	if err != nil {
		return
	}
	if len(role) > 0 {
		err = setRole(tx, role)
		if err != nil {
			return
		}
	}
	return
}

func rollbackResetRole(tx pgx.Tx) (err error) {
	//Reset role shouldn't be needed since that should be included in rollback. But just to be sure
	resetRole(tx)
	err = tx.Rollback(context.TODO())
	return
}

func (db *Db) RunQueryWithoutResult(sql string, role string, args ...interface{}) (int64, error) {

	tx, err := db.getTxSetRole(role)
	if err != nil {
		return 0, err
	}
	defer rollbackResetRole(tx)

	sql = strings.Replace(sql, ";", "", -1)
	res, err := tx.Exec(context.TODO(), sql, args...)
	if err != nil {
		log.Printf("Query failed: %s\n", err)
		return 0, err
	}
	n := res.RowsAffected()
	commitResetRole(tx)
	return n, nil
}

func (db *Db) RunQuery(sql string, role string, v interface{}, args ...interface{}) error {

	tx, err := db.getTxSetRole(role)
	if err != nil {
		return err
	}
	defer rollbackResetRole(tx)

	sql = strings.Replace(sql, ";", "", -1)
	completeSql := fmt.Sprintf(
		`WITH orig_sql AS 
	(%s) 
	SELECT jsonb_agg(row_to_json(orig_sql.*)) 
	FROM orig_sql;`,
		sql)

	if err = tx.QueryRow(context.Background(), completeSql, args...).Scan(v); err != nil {
		log.Printf("Query failed: %s\n", err)
		return fmt.Errorf("Query failed: ", err.Error())
	}

	commitResetRole(tx)
	return nil
}
