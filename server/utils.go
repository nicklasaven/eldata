/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"eldata/server/postgres"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func verifyToken(headers http.Header) (err error) {
	token, err := GetTokenStringFromHeader(headers)
	if err != nil {
		return err
	}

	//Here we misuse the token mechanisms. Since we have full control over both sender and receiver and no one else will be onvolved
	// we just add something as token and check that the same text is comming on each request to the server. There is no reason to
	//put effort in signing the token since the content will not change so we can just verify the full token as is.
	if token != "ThisIsTheSameTextAsFoundInThOtherEndOfTheCommunication" {
		return fmt.Errorf("Ogiltig login")
	}
	return
}

func GetTokenStringFromHeader(headers http.Header) (tokenString string, err error) {
	tokenParts := strings.Fields(headers.Get("Authorization"))
	if len(tokenParts) < 2 {
		return tokenString, fmt.Errorf("Ogiltig login")
	}
	tokenString = tokenParts[1]
	return tokenString, err
}

func readConfig(fileName string) (conf postgres.DBConf, err error) {
	configBytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		return
	}
	err = json.Unmarshal(configBytes, &conf)
	return

}
