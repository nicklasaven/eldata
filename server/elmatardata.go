/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"encoding/json"
	"github.com/google/uuid"
	"io/ioutil"
	"log"
	"net/http"
)

type val struct {
	Val  float32 `json:"val"`
	Unit string  `json:"unit"`
}
type matarData struct {
	V00100  string `json:"0-0:1.0.0"`
	V10180  val    `json:"1-0:1.8.0"`
	V10280  val    `json:"1-0:2.8.0"`
	V10380  val    `json:"1-0:3.8.0"`
	V10480  val    `json:"1-0:4.8.0"`
	V10170  val    `json:"1-0:1.7.0"`
	V10270  val    `json:"1-0:2.7.0"`
	V10370  val    `json:"1-0:3.7.0"`
	V10470  val    `json:"1-0:4.7.0"`
	V102170 val    `json:"1-0:21.7.0"`
	V104170 val    `json:"1-0:41.7.0"`
	V106170 val    `json:"1-0:61.7.0"`
	V102270 val    `json:"1-0:22.7.0"`
	V104270 val    `json:"1-0:42.7.0"`
	V106270 val    `json:"1-0:62.7.0"`
	V102370 val    `json:"1-0:23.7.0"`
	V104370 val    `json:"1-0:43.7.0"`
	V106370 val    `json:"1-0:63.7.0"`
	V102470 val    `json:"1-0:24.7.0"`
	V104470 val    `json:"1-0:44.7.0"`
	V106470 val    `json:"1-0:64.7.0"`
	V103270 val    `json:"1-0:32.7.0"`
	V105270 val    `json:"1-0:52.7.0"`
	V107270 val    `json:"1-0:72.7.0"`
	V103170 val    `json:"1-0:31.7.0"`
	V105170 val    `json:"1-0:51.7.0"`
	V107170 val    `json:"1-0:71.7.0"`
}

type sendDataSimple struct {
	TheTime         int64   `json:"created"`
	MeasuredBought  float32 `json:"measured_bought"`
	MeasuredSold    float32 `json:"measured_sold"`
	PowerBuying     float32 `json:"power_buying"`
	PowerSelling    float32 `json:"power_selling"`
	BoughtThisMonth float32 `json:"bought_this_month"`
	SoldThisMonth   float32 `json:"sold_this_month"`
	BoughtToday     float32 `json:"bought_today"`
	SoldToday       float32 `json:"sold_today"`
}
type clientMeta struct {
	stopSignal int //non zero stop signal means unregister the channel
	clientId   uuid.UUID
}
type serverPack struct {
	dataOutChan chan sendDataSimple
	meta        clientMeta
}

func (p *GoServer) elMatarData(w http.ResponseWriter, r *http.Request) {
	log.Printf("getting data\n")
	err := verifyToken(r.Header)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var res matarData
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		log.Printf("Failed to unmarschal into res. Err :%s\n", err.Error())
	} else {
		log.Printf("data: %v\n", res)
	}

	err = p.sendMatardata(res)
	if err != nil {
		log.Printf("Failed to post to db. Err: %s\n", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	go p.BroadcastSimpleData()
}

func (p *GoServer) BroadcastSimpleData() {
	d, err := p.getDbData()
	if err != nil {
		log.Printf("Failed to fetch data\n")
		return
	}

	p.dataChan <- d

}
