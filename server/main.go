/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"flag"
	"fmt"
	"github.com/google/uuid"
	"gopkg.in/natefinch/lumberjack.v2"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
)

type AsyncServer struct {
	Handler http.Handler
	servers []*http.Server
	Ports   []int
	stop    chan os.Signal
	lock    sync.RWMutex
}

func main() {

	var logTo *string = flag.String("log", "", "-log Where to output log")
	var configFile *string = flag.String("config", "", "-config FileName with db-config")
	flag.Parse()
	if len(*logTo) > 0 {
		fmt.Printf("Log To %s\n", *logTo)
		log.SetOutput(&lumberjack.Logger{
			Filename:   *logTo,
			MaxSize:    5, // megabytes
			MaxBackups: 3,
			MaxAge:     28,   //days
			Compress:   true, // disabled by default
		})
	} else {
		fmt.Printf("No logfile given. Logging to std")
	}
	var err error

	var port string = "8181"

	log.Printf("\nSTARTING UP EL SERVER\n")

	elServer, err := newGoServer(*configFile)

	if err != nil {
		log.Fatalf("Unable to start the server: %v", err.Error())
		return
	}

	log.Printf("Starting up at port %s", port)

	intPort, err := strconv.Atoi(port)

	if err != nil {
		log.Fatalf("invalid port number: %v", port)
	}
	server := newAsyncServer(elServer.router(), intPort)

	server.StartSync()
}

type GoServer struct {
	serverChan chan serverPack
	dataChan   chan sendDataSimple
}

func newGoServer(fileName string) (*GoServer, error) {
	var gs GoServer

	err := SetupElDb(fileName)
	if err != nil {
		return nil, err
	}
	serverChan := make(chan serverPack)
	dataChan := make(chan sendDataSimple)
	gs.serverChan = serverChan
	gs.dataChan = dataChan
	go server(gs.serverChan, gs.dataChan)
	return &gs, nil
}
func newAsyncServer(handler http.Handler, ports ...int) *AsyncServer {
	a := AsyncServer{
		Handler: handler,
		Ports:   ports,
		servers: []*http.Server{},
	}
	return &a
}

func server(serverChan chan serverPack, dataChan chan sendDataSimple) {
	var clients = make(map[uuid.UUID](chan sendDataSimple))
	for {
		select {
		case client, _ := <-serverChan:
			if client.meta.stopSignal != 0 {
				log.Printf("Delete client %s", client.meta.clientId)
				delete(clients, client.meta.clientId)
			} else {
				log.Printf("Add client %s", client.meta.clientId)
				clients[client.meta.clientId] = client.dataOutChan
			}
		case data, _ := <-dataChan:
			log.Printf("N clients: %d\n", len(clients))
			for _, c := range clients {
				c <- data
			}
		}
	}
}

func (a *AsyncServer) StartSync() {
	// Did not lock
	newServer := &http.Server{
		Addr:    ":" + strconv.Itoa(a.Ports[0]),
		Handler: a.Handler,
	}
	a.servers = append(a.servers, newServer)
	if err := newServer.ListenAndServe(); err != nil {
		log.Printf("Server listen and serve: " + err.Error())
	}
}
