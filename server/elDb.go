/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"eldata/server/postgres"
)

type ElDb struct {
	Db *postgres.Db
}

var elDb ElDb

func SetupElDb(fileName string) error {

	var db = postgres.Db{}
	dbConf, err := readConfig(fileName)
	if err != nil {
		return err
	}
	db.DbConf = &dbConf
	elDb.Db = &db
	err = elDb.Db.SetDbConf()
	if err != nil {
		return err
	}

	err = elDb.Db.OpenDb()
	if err != nil {
		return err
	}

	return nil
}

func (p *GoServer) sendMatardata(data matarData) (err error) {

	sql := `
insert into elmatare.matdata("0-0:1.0.0","1-0:1.7.0","1-0:1.8.0","1-0:2.7.0","1-0:2.8.0","1-0:21.7.0","1-0:22.7.0","1-0:23.7.0","1-0:24.7.0","1-0:3.7.0","1-0:3.8.0","1-0:31.7.0","1-0:32.7.0","1-0:4.7.0","1-0:4.8.0","1-0:41.7.0","1-0:42.7.0","1-0:43.7.0","1-0:44.7.0","1-0:51.7.0","1-0:52.7.0","1-0:61.7.0","1-0:62.7.0","1-0:63.7.0","1-0:64.7.0","1-0:71.7.0","1-0:72.7.0")
values 
    (to_timestamp($1, 'YYMMDDHH24MISS')::timestamp,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27) ON CONFLICT DO NOTHING`

	_, err = elDb.Db.RunQueryWithoutResult(sql, elDb.Db.DbConf.DBWriteUser, data.V00100, data.V10170.Val, data.V10180.Val, data.V10270.Val, data.V10280.Val, data.V102170.Val, data.V102270.Val, data.V102370.Val, data.V102470.Val, data.V10370.Val, data.V10380.Val, data.V103170.Val, data.V103270.Val, data.V10470.Val, data.V10480.Val, data.V104170.Val, data.V104270.Val, data.V104370.Val, data.V104470.Val, data.V105170.Val, data.V105270.Val, data.V106170.Val, data.V106270.Val, data.V106370.Val, data.V106470.Val, data.V107170.Val, data.V107270.Val)
	return
}

func (p *GoServer) getDbData() (senddata sendDataSimple, err error) {
	//sql := `SELECT round(EXTRACT(epoch FROM created))::bigint AS created,measured_bought,measured_sold,power_buying,power_selling FROM elmatare.selected order by created desc limit 1;`

	sql := `with this_month as
(SELECT 
max(measured_bought) - min(measured_bought) bought_this_month,
max(measured_sold) - min(measured_sold) sold_this_month 
from elmatare.selected where created_epoch > extract (epoch from date_trunc('month', now()))  and measured_bought > 0 and measured_sold > 0
),
today as
(
SELECT 
max(measured_bought) - min(measured_bought) bought_today,
max(measured_sold) - min(measured_sold) sold_today 
from elmatare.selected where created_epoch > extract (epoch from date_trunc('day', now())) and measured_bought > 0 and measured_sold > 0
)
, now as
(SELECT round(EXTRACT(epoch FROM created))::bigint AS created,measured_bought,measured_sold,power_buying,power_selling FROM elmatare.selected order by created desc limit 1)
select * from now, this_month, today`
	var res []sendDataSimple
	err = elDb.Db.RunQuery(sql, elDb.Db.DbConf.DBReadUser, &res)
	if err != nil {
		return
	}
	if len(res) > 0 {
		senddata = res[0]
	}
	return
}

func (p *GoServer) getDbDataTimeRange(startTime uint64, stopTime uint64, nVals uint64) (senddata []sendDataSimple, err error) {
	//sql := `SELECT round(EXTRACT(epoch FROM created))::bigint AS created,measured_bought,measured_sold,power_buying,power_selling FROM elmatare.selected WHERE created > to_timestamp($1) and created < to_timestamp($2) order by created;`
	sql := `SELECT created_epoch AS created,measured_bought,measured_sold,power_buying,power_selling FROM elmatare.calc_range($1, $2, $3) order by created;`
	err = elDb.Db.RunQuery(sql, elDb.Db.DbConf.DBReadUser, &senddata, startTime, stopTime, nVals)
	if err != nil {
		return
	}
	return
}
