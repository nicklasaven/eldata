/*
Copyright © 2022 Nicklas Avén (nicklas@storvaln.se)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package main

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strconv"
)

//var upgrader = websocket.Upgrader{} // use default options
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true // Disable CORS for testing
	},
}

type errorResponse struct {
	Error string `json:"error"`
}

func (p *GoServer) getDataTimeRange(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	startTime := r.URL.Query().Get("startTime")
	stopTime := r.URL.Query().Get("stopTime")
	nVals := r.URL.Query().Get("nVals")

	startTimeEpoch, err := strconv.ParseUint(startTime, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	stopTimeEpoch, err := strconv.ParseUint(stopTime, 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var nValsInt uint64
	if nVals == "" {
		nValsInt = 60
	} else {
		nValsInt, err = strconv.ParseUint(nVals, 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
	res, err := p.getDbDataTimeRange(startTimeEpoch, stopTimeEpoch, nValsInt)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	resBytes, err := json.Marshal(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(resBytes)

}

func (p *GoServer) getData(w http.ResponseWriter, r *http.Request) {
	var err error
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer p.closeWS(c)
	client := make(chan sendDataSimple, 1)
	clientId_bytes := uuid.New()
	clientId := clientId_bytes
	if err != nil {
		//TODO
	}
	sp := serverPack{client, clientMeta{stopSignal: 0, clientId: clientId}}
	p.serverChan <- sp
	for {
		select {
		case json, _ := <-client:
			err = c.WriteJSON(json)
			if err != nil {
				sp := serverPack{nil, clientMeta{1, clientId}}
				p.serverChan <- sp
				p.closeWS(c)
			}
		}
	}
}
func (p *GoServer) closeWS(c *websocket.Conn) {
	//TODO unregister client
	c.Close()
}

func (p *GoServer) pushData(w http.ResponseWriter, r *http.Request) {
	go p.BroadcastSimpleData()
}
func (p *GoServer) elData(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(homeTemplate))
}

var homeTemplate string = `
<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/date-fns/1.30.1/date_fns.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-date-fns/dist/chartjs-adapter-date-fns.bundle.min.js"></script>
<link rel="stylesheet" href="https://unpkg.com/js-datepicker/dist/datepicker.min.css">

  </head>
  <body>
  <table>
<tr><th>Datum</th><th>timme</th><th>minut</th></td><td></td><td width = 300px><td></td><th colspan=2>Mätarställning</th><th colspan=2>Denna Månad</th><th colspan=3>Idag</th></tr>
<tr><td><input  id="startDate"></td><td><select type="text" id="startHour"></td><td><select type="text" id="startMinute"></td><td></td><td width = 300px></td><th>köp:</th><td id="mkop"></td><th>kWh</th><td id="monthkop"></td><th>kWh</th><td id="todaykop"></td><th>kWh</th></tr>
 <tr><td><input  id="stopDate"></td><td><select type="text" id="stopHour"></td><td><select type="text" id="stopMinute"></td><td><input type="button" value="set stop to now" onClick="setStopToNow()"></td><td width = 300px></td><th>sälj:</th><td id="msalj"></td><th>kWh</th><td id="monthsalj"></td><th>kWh</th><td id="todaysalj"></td><th>kWh</th></tr>
 <tr><td colspan=3><input type="button" value="OK" onClick="getNewData()" ></td></tr>
 <table>
 
 
 <input id="nValsRange" type="range" value=360 min="10" max="3600" onChange="numVals()">
<canvas id="myChart" width="400" height="200"></canvas>
    <script src="https://unpkg.com/js-datepicker"></script>
<script>
//const baseUrl = "localhost:8181"
const baseUrl = "Set.Your.HostName.Here"

      var numsamples = 360;
      var updateRealTime = 1;
      
var options = {id:1,formatter: (input, date, instance) => {
    const value = date.toLocaleString('SE');
    input.value = value // => '1/1/2099'
  }};

/**/
const start = datepicker(document.getElementById("startDate"), options);
const end = datepicker(document.getElementById("stopDate"), options);
function numVals()
{
	
	var val = document.getElementById("nValsRange").value;
	numsamples = val;
	getNewData();
}
function stopws()
{
dataSocket.close();
}

var d = new Date();
document.getElementById("startDate").value = d.toISOString().substring(0,10);
document.getElementById("startHour").selected = d.getUTCHours();
document.getElementById("startMinute").selectedIndex = d.getMinutes();

function setStopToNow()
{

	var d = new Date();
var d = new Date()
document.getElementById("stopDate").value = d.toISOString().substring(0,10);
document.getElementById("stopHour").selected = d.getUTCHours();
document.getElementById("stopMinute").selectedIndex = d.getMinutes();
}
function getNewData()
{
	var startDateTxt = document.getElementById("startDate").value;
	var startDateEpoch = new Date(startDateTxt).getTime()/1000;
	var startHourSel = document.getElementById("startHour");
	var startHour = startHourSel.options[startHourSel.selectedIndex].value;
	var startMinuteSel = document.getElementById("startMinute");
	var startMinute = startMinuteSel.options[startMinuteSel.selectedIndex].value;
	var stopDateTxt = document.getElementById("stopDate").value;
	var stopDateEpoch = new Date(stopDateTxt).getTime()/1000;
	var stopHourSel = document.getElementById("stopHour");
	var stopHour = stopHourSel.options[stopHourSel.selectedIndex].value;
	var stopMinuteSel = document.getElementById("stopMinute");
	var stopMinute = stopMinuteSel.options[stopMinuteSel.selectedIndex].value;
	var startEpoch = 1 * startDateEpoch + 3600 * startHour + 60 * startMinute;
	var stopEpoch = 1 * stopDateEpoch + 3600 * stopHour + 60 * stopMinute;
	if (startEpoch != startEpoch || stopEpoch != stopEpoch ) //checking for NaN
		return
	getTimeRangeData(Math.round(startEpoch), Math.round(stopEpoch))
	
	var currentDate = new Date();
	var nu =  currentDate.getTime();
	if ( nu/1000 - stopEpoch > 120)
	{	
		updateRealTime = 0;
	}else{
		updateRealTime = 1;
	}
}
function getTimeRangeData(startEpoch, stopEpoch)
{

	var requestObject = {};	
	requestObject.url = "getDataTimeRange?startTime=" + startEpoch + "&stopTime=" + stopEpoch + "&nVals=" + numsamples;
	requestObject.callback=haveData;
	getData(requestObject);
}
function haveData(res)
{
	var array_len = res.length;
	ElData.datasets[0].data = [];
	ElData.datasets[1].data = [];
	ElData.labels = [];
	var unfilled = numsamples - array_len
	for (i=numsamples - array_len; i>0; i--)
	{
		ElData.datasets[0].data.push({y:null,x:res[0].created * 1000 - i * 10000});
		ElData.datasets[1].data.push({y:null,x:res[0].created * 1000 - i * 10000});
	}
	for (i=0; i<array_len; i++)
	{
		if (i % 10 == 0)
		{		
		   ElData.labels.push(res[i].created * 1000);
		}		
		ElData.datasets[0].data.push({y:res[i].power_selling,x:res[i].created * 1000});
		ElData.datasets[1].data.push({y:res[i].power_buying,x:res[i].created * 1000});
	}
	
	ElDiagram.update();
	
}

function getData(requestObject)
{
    
    if (requestObject.maxAntal)	
        var maxAntal = requestObject.maxAntal-1;
    else
        var maxAntal = 9;
   if (requestObject.payload)
    {	
        var method = 'POST';
    }
    else
    {
        var method = 'GET';	
    }

     var xhr = new XMLHttpRequest();
     var onError = function() {
       alert("problem");
     }
     xhr.onerror = onError;
	var url;
if (baseUrl.substring(0,9) == "localhost")
	url  = "http://" + baseUrl + "/" + requestObject.url;
else
	url = "https://" + baseUrl + "/" + requestObject.url;

    xhr.open(method,url,true);
    if (requestObject.bearer)
    {	
	xhr.setRequestHeader('Authorization', 'bearer ' + requestObject.bearer);
    }

     xhr.onload = function() {
       if (xhr.status == 200) {
                 var rawdata=xhr.responseText;
               requestObject.callback(JSON.parse(rawdata));	
       } else {
         onError();
       }
     }
     
     if(requestObject.payload)
        xhr.send(JSON.stringify(payload));
    else
        xhr.send(null);
}
function addSelMinutes(id, start, end)
{
	sel = document.getElementById(id);
	for (i=start;i<end; i++)
	{
	  var option = document.createElement("option");
	  option.value = i;
	  option.text = i;
	  sel.add(option);
	}
}
function addSelHours(id, start, end)
{
	d = new Date();
	var h = d.getHours();
	var t = d.getTime() - h * 3600 + 12 * 3600;
	var UtcLocalDiff = d.getHours(t) - d.getUTCHours(t);
	sel = document.getElementById(id);
	r = d.getUTCHours();
	for (i=start;i<end; i++)
	{
	  var option = document.createElement("option");
	  option.value = i-UtcLocalDiff;
	  option.text = i;
	  sel.add(option);
	}
}
addSelHours("startHour",0,24);
addSelMinutes("startMinute",0,60);
addSelHours("stopHour",0,24);
addSelMinutes("stopMinute",0,60);

var d = new Date(new Date().getTime() - numsamples * 10000);
document.getElementById("startDate").value = d.toISOString().substring(0,10);
document.getElementById("startHour").value = d.getUTCHours();
document.getElementById("startMinute").value = d.getMinutes();


const ctx = document.getElementById('myChart');


var AvgCpuChartOptions = {
  showLines: true,
  animation: {
    duration: 0,
    easing: 'linear'
  },
  //responsive: true,
  title: {
    display: true,
    text: 'Average CPU Usage (%)',
    padding: 5
  },
  legend: {
    display: true,
    position: 'top',
/*    labels: {
      boxWidth: 10,
      padding: 2
    }*/
  },
  tooltips: {
    enabled: true
  },
  scales: {
    y: {
	min: 0,
	max: 12
	},
    x: {
        type: 'time',
        time: {
          // Luxon format string
          //tooltipFormat: 'dd T'
        },
        title: {
          display: true,
          text: 'Date'
        },
	ticks: {
		display: true,
          autoSkip: true,
          maxRotation: 45,
          major: {
            enabled: true
          },
          // color: function(context) {
          //   return context.tick && context.tick.major ? '#FF0000' : 'rgba(0,0,0,0.1)';
          // },
          font: function(context) {
            if (context.tick && context.tick.major) {
              return {
                weight: 'bold',
              };
            }
    }
      }
}}};

      var ts = new Date().getTime();
var ElData = {
  labels: [],
  datasets: [{
            label: 'Effekt som säljs',
    fill: false,
    lineTension: 0.2,
    backgroundColor: 'rgba(0,150,0,1)',
    borderColor: 'rgba(0,150,0,1)',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderWidth: 3,
    borderJoinStyle: 'miter',
    pointBorderColor: 'rgba(0,150,0,1)',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 0,
    pointHoverBackgroundColor: 'rgba(0,150,0,1)',
    pointHoverBorderColor: 'rgba(220,220,220,1)',
    pointHoverBorderWidth: 0,
    pointRadius: 0,
    pointHitRadius: 10,
    data: [],
  },
  {
            label: 'Effekt som köps',
    fill: false,
    lineTension: 0.2,
    backgroundColor: 'rgba(150,0,0,1)',
    borderColor: 'rgba(150,0,0,1)',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderWidth: 3,
    borderJoinStyle: 'miter',
    pointBorderColor: 'rgba(150,0,0,1)',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 0,
    pointHoverBackgroundColor: 'rgba(150,0,0,1)',
    pointHoverBorderColor: 'rgba(220,220,220,1)',
    pointHoverBorderWidth: 0,
    pointRadius: 0,
    pointHitRadius: 10,
    data: [],
  }]
};

for (var i = 0; i < numsamples; i++) {
//	if (i % 10000 == 0)
//		ElData.labels.push(ts-10000*(numsamples-i));		
  ElData.datasets[0].data.push({x:ts-10000*(numsamples-i), y:null});
  ElData.datasets[1].data.push({x:ts-10000*(numsamples-i), y:null});
}

const ElDiagram = new Chart(ctx, {
    type: 'line',
    data: ElData,
    options: AvgCpuChartOptions
});



getTimeRangeData(Math.round(new Date().getTime()/1000 - numsamples * 10),Math.round(new Date().getTime()/1000));

var socketUrl;
if (baseUrl.substring(0,9) == "localhost")
	socketUrl = "ws://" + baseUrl + "/getData";
else
	socketUrl = "wss://" + baseUrl + "/getData";
	
const dataSocket = new WebSocket(socketUrl);


dataSocket.onmessage = (event) => {
if (!updateRealTime)
	return;
if(ElData.datasets[0].data.length > numsamples)
{
      ElData.datasets[0].data.shift();
      ElData.datasets[1].data.shift();
      ElData.labels.shift();
}
      var parsedData = JSON.parse(event.data);
	ElData.datasets[0].data.push({y:parsedData.power_selling,x:parsedData.created * 1000});
	ElData.datasets[1].data.push({y:parsedData.power_buying,x:parsedData.created * 1000});
        if (parsedData.created % 100000 == 0)
		ElData.labels.push(parsedData.created * 1000);

	ElDiagram.update();
	
	document.getElementById("mkop").innerText = parsedData.measured_bought;
	document.getElementById("msalj").innerText = parsedData.measured_sold;
	document.getElementById("monthkop").innerText = parsedData.bought_this_month;
	document.getElementById("monthsalj").innerText = parsedData.sold_this_month;
	document.getElementById("todaykop").innerText = parsedData.bought_today;
	document.getElementById("todaysalj").innerText = parsedData.sold_today;
	
	var d = new Date();
	document.getElementById("stopDate").value = d.toISOString().substring(0,10);
	document.getElementById("stopHour").value = d.getUTCHours();
	document.getElementById("stopMinute").value = d.getMinutes();
	console.log(event.data);
	console.log("array len: " + ElData.datasets[0].data.length);
}


    </script>
  </body>

</html>
`
