**Ett litet projekt för att samla och visa elmätardata**

Efter att ha skaffat solceller och fått en ny sk "smart" mätare från Ellevio så uppkom möjligheten att samla in data direkt från mätaren. Vi kan då i en web-sida se både köp och försäljning av el var 10:e sekund.

De nya elmätarna har ett mer eller mindre standardiserat interface för att läsa data.
https://www.energiforetagen.se/globalassets/energiforetagen/det-erbjuder-vi/kurser-och-konferenser/elnat/branschrekommendation-lokalt-granssnitt-v2_0-201912.pdf

Inspiration till detta har kommit bla från denna blog-posten. Det är förövrigt en likadan mätare som vi har fått
https://www.akehedman.se/wordpress/?p=38542

Det finns naturligtvis oändligt många sätt att göra detta på, men det jag önskade är att samla all data och spara den för att sedan kunna jämföra olika tidsperioder i efterhand och gräva djupare i data. Jag har därför valt att skicka allt till en databas.


**Material**

En adapter från rj12-kontakt till USB som jag köpte här:
https://energyintelligence.se/shop/product/p1-han-till-usb-adapter

En vanlig Raspberry Pi 3 med Wifi.
Det går bra att använda vilken dator som helst som kan kompilera C, har usb-port och kan kopplas till ett nätverk. Men det underlätar om det finns färdiga paket med libserial och libcurl, annars får man bygga dem också.

kod för rpin samt README finns i foldern rpi 


**Server**

Man behöver någon form av dator somär åtkomlig dygnet runt. Jag har en liten server i molnet från Linode.com, men man kan ha en dator stående hemma också, eller annan molntjänst.
Man skulle också kunna lagra data i raspberrryn men man riskerar att minneskortet blir fullt, eller slitet och slutar fungera då mycket data kommer att skrivas till det.

kod för rpin samt README finns i foldern server 


**Databas**

Jag har valt min favoritdatabas PostgreSQL. Den funkar till allt, men man kan naturligtvis lika gärna använda sqlite, eller någon annan databas. Men med PostgreSQL så har jag alla möjligheter för framtiden. Jag har också valt att köra den på samma server som jag har satt upp go-servern på. Då kan jag koppla upp mot databasen med en unix-socket och behöver inte använda passord. Allt sker innanför samma servers väggar.

sql-kod för databasen finns i fodern db




**Tillslut**

Detta projekt är bara skrivet för mina egna behov. Denna lilla dokumentation är bristfällig och mycket av koden är bristfällig och slarvig. Men det verkar tjäna sitt syfte ch har du nytta av något av det så är det ju bara bra.
